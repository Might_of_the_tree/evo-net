namespace EVO{

    struct creature{

        bool bNumonfield;

        char direction;

        unsigned char u_chEnergie;

        int iGenetic;

        NNET::neural_network Brain;
    
        MAP::koordinates position_on_field;
        MAP::koordinates position;

        creature(){};

    };

    void evolution(const bool &bRun, MAP::map &Evo_Map, C_vector<EVO::creature> &creatures);
    void birth(MAP::map &Evo_Map, C_vector<EVO::creature> &creatures, EVO::creature &iMother);
    void eat(MAP::map &Evo_Map, EVO::creature &eating);
    void attack(MAP::map &Evo_Map, C_vector<EVO::creature> creatures, EVO::creature &attacker);
    void move(MAP::map &Evo_Map, EVO::creature &walking);
    void turn_right(EVO::creature &turning);
    void turn_left(EVO::creature &turning);
    void die(MAP::map &Evo_Map, C_vector<EVO::creature> creatures, EVO::creature &die, int &iDie);

}