#include "./include.hpp"



extern D_vector<int> GLOiVfree_places;
extern int GLOifree_index;

C_vector<D_vector<int>>* get_Connections();
MAP::koordinates* startup(MAP::map &Evo_Map);



int main(){
    
    bool bRun;

    int i;
    //start of creating the start attributes for the simulation
    D_vector<int> Neurons(4);

    MAP::map Evo_Map(5000, 5000);

    MAP::koordinates* spawn=startup(Evo_Map);

    while(spawn == nullptr){

        Evo_Map.rebuild();
        spawn=startup(Evo_Map);

    }

    

    C_vector<EVO::creature> creatures(200);
    //mark all free places in the vector creatures
    for(i=0; i < 100; i++){

        GLOiVfree_places[i]=100+i;

    }

    GLOifree_index=99;

    //set input number to 5
    Neurons[1]=5;
    //set output Number to 6
    Neurons[3]=5;

    C_vector<C_vector<D_vector<int>>> Connections(0);
    
    //set the size
    Connections.iSize=30;
    //set the data
    Connections.ValPnt = get_Connections();

    for(i=0; i < 100; i++){

        new(&creatures[i].Brain) NNET::neural_network(Neurons, Connections);
        creatures[i].iGenetic=0;
        creatures[i].u_chEnergie=200;
        creatures[i].direction=0;
        creatures[i].position.x=(*(spawn+i)).x;
        creatures[i].position.y=(*(spawn+i)).y;
        creatures[i].position_on_field.x=0;
        creatures[i].position_on_field.y=0;

    }

    EVO::evolution(bRun, Evo_Map, creatures);

    return 0;
}

C_vector<D_vector<int>>* get_Connections(){

    int i;

    C_vector<C_vector<D_vector<int>>> Connections(4);

    //make space for the vectors wich represent the Connections
    Connections[1].push_back(25);

    for(i=0; i < 25; i++){

        Connections[1][i].push_back(4);

    }

    //define the connection begin and end manual
    Connections[1][ 0][0]=0;
    Connections[1][ 0][1]=1;
    Connections[1][ 0][2]=0;
    Connections[1][ 0][3]=3;

    Connections[1][ 1][0]=0;
    Connections[1][ 1][1]=1;
    Connections[1][ 1][2]=1;
    Connections[1][ 1][3]=3;

    Connections[1][ 2][0]=0;
    Connections[1][ 2][1]=1;
    Connections[1][ 2][2]=2;
    Connections[1][ 2][3]=3;

    Connections[1][ 3][0]=0;
    Connections[1][ 3][1]=1;
    Connections[1][ 3][2]=3;
    Connections[1][ 3][3]=3;

    Connections[1][ 4][0]=0;
    Connections[1][ 4][1]=1;
    Connections[1][ 4][2]=4;
    Connections[1][ 4][3]=3;



    Connections[1][ 5][0]=0;
    Connections[1][ 5][1]=1;
    Connections[1][ 5][2]=0;
    Connections[1][ 5][3]=3;

    Connections[1][ 6][0]=0;
    Connections[1][ 6][1]=1;
    Connections[1][ 6][2]=1;
    Connections[1][ 6][3]=3;

    Connections[1][ 7][0]=0;
    Connections[1][ 7][1]=1;
    Connections[1][ 7][2]=2;
    Connections[1][ 7][3]=3;

    Connections[1][ 8][0]=0;
    Connections[1][ 8][1]=1;
    Connections[1][ 8][2]=3;
    Connections[1][ 8][3]=3;

    Connections[1][ 9][0]=0;
    Connections[1][ 9][1]=1;
    Connections[1][ 9][2]=4;
    Connections[1][ 9][3]=3;




    Connections[1][10][0]=0;
    Connections[1][10][1]=1;
    Connections[1][10][2]=0;
    Connections[1][10][3]=3;

    Connections[1][11][0]=0;
    Connections[1][11][1]=1;
    Connections[1][11][2]=1;
    Connections[1][11][3]=3;

    Connections[1][12][0]=0;
    Connections[1][12][1]=1;
    Connections[1][12][2]=2;
    Connections[1][12][3]=3;

    Connections[1][13][0]=0;
    Connections[1][13][1]=1;
    Connections[1][13][2]=3;
    Connections[1][13][3]=3;

    Connections[1][14][0]=0;
    Connections[1][14][1]=1;
    Connections[1][14][2]=4;
    Connections[1][14][3]=3;



    Connections[1][15][0]=0;
    Connections[1][15][1]=1;
    Connections[1][15][2]=0;
    Connections[1][15][3]=3;

    Connections[1][16][0]=0;
    Connections[1][16][1]=1;
    Connections[1][16][2]=1;
    Connections[1][16][3]=3;

    Connections[1][17][0]=0;
    Connections[1][17][1]=1;
    Connections[1][17][2]=2;
    Connections[1][17][3]=3;

    Connections[1][18][0]=0;
    Connections[1][18][1]=1;
    Connections[1][18][2]=3;
    Connections[1][18][3]=3;

    Connections[1][19][0]=0;
    Connections[1][19][1]=1;
    Connections[1][19][2]=4;
    Connections[1][19][3]=3;


    Connections[1][20][0]=0;
    Connections[1][20][1]=1;
    Connections[1][20][2]=0;
    Connections[1][20][3]=3;

    Connections[1][21][0]=0;
    Connections[1][21][1]=1;
    Connections[1][21][2]=1;
    Connections[1][21][3]=3;

    Connections[1][22][0]=0;
    Connections[1][22][1]=1;
    Connections[1][22][2]=2;
    Connections[1][22][3]=3;

    Connections[1][23][0]=0;
    Connections[1][23][1]=1;
    Connections[1][23][2]=3;
    Connections[1][23][3]=3;

    Connections[1][24][0]=0;
    Connections[1][24][1]=1;
    Connections[1][24][2]=4;
    Connections[1][24][3]=3;

    //do this so the data wont be deleted
    Connections.iSize=0;
    //return a pointer to the data
    return Connections.ValPnt;

}

MAP::koordinates* startup(MAP::map &Evo_Map){

    bool found=true;

    int middle1=Evo_Map.map_build.iSize/2;
    int middle2=Evo_Map.map_build[0].iSize/2;
    int middleh1=Evo_Map.map_build.iSize/2;
    int middleh2=Evo_Map.map_build[0].iSize/2;
    int i;
    int i1;

    //get a spawn
    static MAP::koordinates spawn[100];

    //search 100 places
    for(i=0; i < 100; i++){
        found=true;
        //search until koordinates are found
        while(found){
            //ask if the creatures are beside water and if thei are not in water
            if(Evo_Map.map_build[middle1][middle2].bWater==false && Evo_Map.map_build[middle1][middle2].bBesideWater==true){
                //get the information if those places have already been taken
                for(i1=0; i1 < i; i1++){
                    
                    if(spawn[i1].x == middle1 && spawn[i1].y == middle2){

                        break;

                    }else{

                        spawn[i].x=middle1;
                        spawn[i].y=middle2;
                        found=false;

                    }

                }

                //change the y Layer
                middle2++;
                //check if the y Layer is at the end
                if(middle2 > Evo_Map.map_build[0].iSize){
                    //change x Layer
                    middle1++;
                    middle2=0;

                }

            }else{
                
                //check if the algoritm was already on this place
                if(middle1 == middleh1 && middle2 == middleh2){

                    return nullptr;

                }
                //check if the algoritm is on the end of the map
                if(middle1 > Evo_Map.map_build.iSize){
                    //set the Layer to 0
                    middle1=0;

                }

                //change the y Layer
                middle2++;
                //check if the y Layer is at the end
                if(middle2 > Evo_Map.map_build[0].iSize){
                    //change x Layer
                    middle1++;
                    middle2=0;

                }

            }

        }

    }

    return &spawn[0];

}
