//Container with Data types
template<typename T>
struct D_vector{
	
	T* ValPnt;
	int iSize;
	
	D_vector();
	D_vector(int iSize);
	~D_vector();
	D_vector(D_vector<T> &&);
	
	T& operator[](int iIndex);
	D_vector<T>& operator+(const D_vector<T> &other);
	D_vector<T>& operator-(const D_vector<T> &other);
	void operator-=(const D_vector<T> &other);
	void operator+=(const D_vector<T> &other);
	void operator=(const D_vector<T> &other);
	
	void push_back(int iIndex);
	void deleteback(int iIndex);
	void push_front(int iIndex);
	void deletefront(int iIndex);
	void delete_(int iPlace);
	
};


//Container with classes
template<class T>
struct C_vector{
	
	T* ValPnt;
	int iSize;
	
	C_vector();
	C_vector(int iSize);
	~C_vector();
	
	T& operator[](int iIndex);
	void operator=(const C_vector<T> &other);
	
	void push_back(int iIndex);
	void deleteback(int iIndex);
	void push_front(int iIndex);
	void deletefront(int iIndex);
	void delete_(int iPlace);
	
};
