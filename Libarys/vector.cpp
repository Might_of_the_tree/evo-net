#include <cstring>


template<typename T>
D_vector<T>::D_vector(){

	iSize=0;

}

template<typename T>
D_vector<T>::D_vector(int iSize)
: iSize(iSize){
		
	//Create an array to save the data
	ValPnt = new T[iSize];
		
}

template<typename T>
D_vector<T>::D_vector(D_vector<T> &&other){
	
	other.ValPnt = ValPnt;
	other.iSize=iSize;
	ValPnt=nullptr;
	
}

template<typename T>
D_vector<T>::~D_vector(){
	
	int i;
	
	for(i=0; i < iSize; i++){
		
		//delete data
		delete (ValPnt+i);
		
	}
	
}

template<typename T>
T& D_vector<T>::operator[](int iIndex){
	
	return ValPnt[iIndex];
	
}

template<typename T>
void D_vector<T>::operator=(const D_vector<T> &other){

	int i;

	if(other.iSize < iSize){

		this->deletefront((iSize-other.iSize));

		for(i=0; i < iSize; i++){

			*(ValPnt+i) = other[i];

		}

	}else{

		D_vector<T> help(other.iSize);

		for(i=0; i < iSize; i++){
		
			//delete the old array
			delete (ValPnt+i);
		
		}

		memcpy(help.ValPnt, other.ValPnt, (help.iSize*sizeof(T)));

		ValPnt=help.ValPnt;
		iSize=help.iSize;

		help.iSize=0;

	}

}

template<typename T>
D_vector<T>& D_vector<T>::operator+(const D_vector<T> &other){
	
	int i;
	
	
	if(iSize < other.iSize){
		
		D_vector<T> retVal(other.iSize);
	
		for(i=0; i < iSize; i++){
	
			retVal[i]=(*this)[i]+other[i];
	
		}
		
		for(i=iSize; i < other.iSize; i++){
			
			retVal[i]=other[i];
			
		}

		return retVal;
		
	}else{
		
		D_vector<T> retVal(iSize);
	
		for(i=0; i < other.iSize; i++){
	
			retVal[i]=(*this)[i]+other[i];
	
		}
		
		for(i=other.iSize; i < iSize; i++){
			
			retVal[i]=(*this)[i];
			
		}

		return retVal;
		
	}

}

template<typename T>
D_vector<T>& D_vector<T>::operator-(const D_vector<T> &other){
	
	int i;
	
	
	if(iSize < other.iSize){
		
		D_vector retVal(other.iSize);
	
		for(i=0; i < iSize; i++){
	
			retVal[i]=(*this)[i]-other[i];
	
		}
		
		for(i=iSize; i < other.iSize; i++){
			
			retVal[i]=(-1*other[i]);
			
		}

		return retVal;
		
	}else{
		
		D_vector retVal(iSize);
	
		for(i=0; i < other.iSize; i++){
	
			retVal[i]=(*this)[i]-other[i];
	
		}
		
		for(i=other.iSize; i < iSize; i++){
			
			retVal[i]=(*this)[i];
			
		}

		return retVal;
		
	}

}

template<typename T>
void D_vector<T>::operator-=(const D_vector<T> &other){

	int i;

	if(iSize < other.iSize){

		for(i=0; i < iSize; i++){

			*(ValPnt+i) = *(ValPnt+i)-other[i];

		}

	}else{

		for(i=0; i < other.iSize; i++){

			*(ValPnt+i) = *(ValPnt+i)-other[i];

		}

	}

}

template<typename T>
void D_vector<T>::operator+=(const D_vector<T> &other){

	int i;

	if(iSize < other.iSize){

		for(i=0; i < iSize; i++){

			*(ValPnt+i) = *(ValPnt+i)+other[i];

		}

	}else{

		for(i=0; i < other.iSize; i++){

			*(ValPnt+i) = *(ValPnt+i)+other[i];

		}

	}

}

template<typename T>
void D_vector<T>::push_back(int iIndex){
	
	int i;
	
	T* hpValPnt;
	
	//creates a new and bigger array
	hpValPnt = new T[(iSize+iIndex)];
	
	memcpy(hpValPnt, ValPnt, (iSize*sizeof(T)));
	
	for(i=0; i < iSize; i++){
		
		//delete the old array
		delete (ValPnt+i);
		
	}
	
	//change Size
	iSize=+iIndex;
	//makes the new arrray to the default array
	ValPnt = hpValPnt;
	//set the old pointer to a null pointer
	hpValPnt=nullptr;
	
}

template<typename T>
void D_vector<T>::deleteback(int iIndex){
	
	int i;
	//delete and start at the end
	for(i=0; i < iIndex; i++){
		
		delete (ValPnt+(iSize-i));
		
	}
	
	iSize=-iIndex;
}

template<typename T>
void D_vector<T>::push_front(int iIndex){
	
	int i;
	
	T* hpValPnt;
	
	//creates a new and bigger array
	hpValPnt = new T[(iSize+iIndex)];
	
	memcpy(hpValPnt, (ValPnt+iIndex), (iSize*sizeof(T)));
	
	for(i=0; i < iSize; i++){
		
		//delete the old array
		delete (ValPnt+i);
		
	}
	
	//change Size
	iSize=+iIndex;
	//makes the new arrray to the default array
	ValPnt = hpValPnt;
	//set the old pointer to a null pointer
	hpValPnt=nullptr;
	
}

template<typename T>
void D_vector<T>::deletefront(int iIndex){
	
	int i;
	//delete a chosed number of datatypes
	for(i=0; i < iIndex; i++){
		
		delete (ValPnt+i);
		
	}
	
	ValPnt=ValPnt+iIndex;
	iSize=-iIndex;

}

template<typename T>
void D_vector<T>::delete_(int iPlace){
	
	int i;

	delete (ValPnt+iPlace);

	for(i=iPlace; i < iSize; i++){

		*(ValPnt+i)= *(ValPnt+i+1);

	}
	
}





//all is the same as the first class but it is working with classes and not with datatypes
template<class T>
C_vector<T>::C_vector(){

	iSize=0;

}

template<class T>
C_vector<T>::C_vector(int iSize)
: iSize(iSize){
		
	ValPnt = new T[iSize];
		
}

template<class T>
C_vector<T>::~C_vector(){
	
	int i;
	
	for(i=0; i < iSize; i++){
		
		delete (ValPnt+i);
		
	}
	
}

template<class T>
T& C_vector<T>::operator[](int iIndex){
	
	return ValPnt[iIndex];
	
}

template<class T>
void C_vector<T>::operator=(const C_vector &other){

	int i;

	if(other.iSize < iSize){

		this->deletefront((iSize-other.iSize));

		for(i=0; i < iSize; i++){

			*(ValPnt+i) = other[i];

		}

	}else{

		C_vector<T> help(other.iSize);

		for(i=0; i < iSize; i++){
		
			//delete the old array
			delete (ValPnt+i);
		
		}

		memcpy(help.ValPnt, other.ValPnt, (help.iSize*sizeof(T)));

		ValPnt=help.ValPnt;
		iSize=help.iSize;

		help.iSize=0;

	}

}

template<class T>
void C_vector<T>::push_back(int iIndex){
	
	int i;
	
	T* hpValPnt;
	
	//creates a new and bigger array
	hpValPnt = new T[(iSize+iIndex)];
	
	memcpy(hpValPnt, ValPnt, (iSize*sizeof(T)));
	
	for(i=0; i < iSize; i++){
		
		//delete the old array
		delete (ValPnt+i);
		
	}
	
	//change Size
	iSize=+iIndex;
	//makes the new arrray to the default array
	ValPnt = hpValPnt;
	//set the old pointer to a null pointer
	hpValPnt=nullptr;
	
}

template<class T>
void C_vector<T>::deleteback(int iIndex){
	
	int i;
	//delete and start at the end
	for(i=0; i < iIndex; i++){
		
		delete (ValPnt+(iSize-i));
		
	}
	iSize=-iIndex;
}

template<class T>
void C_vector<T>::push_front(int iIndex){
	
	int i;
	
	T* hpValPnt;
	
	//creates a new and bigger array
	hpValPnt = new T[(iSize+iIndex)];
	
	memcpy(hpValPnt, (ValPnt+iIndex), (iSize*sizeof(T)));
	
	for(i=0; i < iSize; i++){
		
		//delete the old array
		delete (ValPnt+i);
		
	}
	
	//change Size
	iSize=+iIndex;
	//makes the new arrray to the default array
	ValPnt = hpValPnt;
	//set the old pointer to a null pointer
	hpValPnt=nullptr;
	
}

template<class T>
void C_vector<T>::deletefront(int iIndex){
	
	int i;
	//delete a chosed number of datatypes
	for(i=0; i < iIndex; i++){
		
		delete (ValPnt+i);
		
	}
	
	ValPnt=ValPnt+iIndex;
	iSize=-iIndex;

}

template<class T>
void C_vector<T>::delete_(int iPlace){
	
	int i;

	delete (ValPnt+iPlace);

	for(i=iPlace; i < iSize; i++){

		*(ValPnt+i)= *(ValPnt+i+1);

	}
	
}
