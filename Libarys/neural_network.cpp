


//set the output with the input in a funktion
void NNET::input_N::set_ftdOutput(){

    //ftdOutput= *ftdPtrInput in a funktion;

}





//just make standart constructor for the D_vector
NNET::hidden_output_N::hidden_output_N(){

    VftdInput.push_back(0);

}

void NNET::hidden_output_N::set_ftdOutput(){

    int i;

    //avoid errors
    ftdOutput=0;

    //add all Inputs
    for(i=0; i < VftdInput.iSize; i++){

        ftdOutput+=VftdInput[i];

    }

    //set output with the sum of all inputs in a funktion

    //ftdOutput= funktion with ftdOutput

}





void NNET::connection::set_Weight(){

    int i;

    ftdWeight=0;

    for(i=0; i < 10; i++){

        ftdWeight+= (*(ftdPtrXi+i))*(*(ftdPtrXj+i));

    }

    ftdWeight/=10;

}

void NNET::connection::set_ftdPtrOutput(){

    *ftdPtrOutput = (*ftdPtrInput)*ftdWeight;

}





NNET::neural_network::neural_network(D_vector<int> &VNeurons, C_vector<C_vector<D_vector<int>>> &VViConnections){

    int i;
    int i1;

    //create space for the neurons
    ftdBias.push_back(VNeurons[0]);
    Vi_N_input.push_back(VNeurons[1]);
    Vh_o_N.push_back(VNeurons.iSize-2);

    for(i=2; i < VNeurons.iSize; i++){

        Vh_o_N[i].push_back(VNeurons[i]);

    }

    //create space to save x of i at the place k
    VVftdX.push_back(VNeurons.iSize);
    
    for(i=0; i < VNeurons.iSize; i++){

        VVftdX[i].push_back(VNeurons[i]);

        for(i1=0; i1 < VNeurons[i]; i1++){

            VVftdX[i][i1].push_back(10);

        }

    }

    //create space for the connections
    Vcnn_connection.push_back(VViConnections.iSize);
    Vcnn_connection[0].push_back(VViConnections[0].iSize);
    Vcnn_connection[2].push_back(VViConnections[1].iSize);


    for(i=0; i < VViConnections[0].iSize; i++){

        //set the variables in the connections
        //set start index
        Vcnn_connection[0][i].iBeg_End[0]=VViConnections[0][i][0];
        //set start Layer
        Vcnn_connection[0][i].iBeg_End[1]=VViConnections[0][i][1];
        //set end index
        Vcnn_connection[0][i].iBeg_End[2]=VViConnections[0][i][2];
        //set end Layer
        Vcnn_connection[0][i].iBeg_End[3]=VViConnections[0][i][3];
        //set the reference to the input                
        Vcnn_connection[0][i].ftdPtrInput= &ftdBias[VViConnections[0][i][0]];
        //set the reference to the output
        Vcnn_connection[0][i].ftdPtrOutput= &Vh_o_N[VViConnections[0][i][3]][VViConnections[0][i][2]].ftdOutput;
        //set the reference to x of i at the place k (Hebbian learning)
        Vcnn_connection[0][i].ftdPtrXi= VVftdX[0][VViConnections[0][i][0]].ValPnt;
        //set the reference to x of j at the place k (Hebbian learning)
        Vcnn_connection[0][i].ftdPtrXj= VVftdX[VViConnections[0][i][3]][VViConnections[0][i][2]].ValPnt;

    }

    for(i=0; i < VViConnections[1].iSize; i++){

        //same as at Layer 86 to 102 but with input Neurons as start
        Vcnn_connection[1][i].iBeg_End[0]=VViConnections[1][i][0];
        Vcnn_connection[1][i].iBeg_End[1]=VViConnections[1][i][1];
        Vcnn_connection[1][i].iBeg_End[2]=VViConnections[1][i][2];
        Vcnn_connection[1][i].iBeg_End[3]=VViConnections[1][i][3];                
        Vcnn_connection[1][i].ftdPtrInput= &Vi_N_input[VViConnections[1][i][0]].ftdOutput;
        Vcnn_connection[1][i].ftdPtrOutput= &Vh_o_N[VViConnections[1][i][3]][VViConnections[1][i][2]].ftdOutput;
        Vcnn_connection[1][i].ftdPtrXi= VVftdX[1][VViConnections[1][i][0]].ValPnt;
        Vcnn_connection[1][i].ftdPtrXj= VVftdX[VViConnections[1][i][3]][VViConnections[1][i][2]].ValPnt;

    }

    for(i=2; i < VViConnections.iSize; i++){

        //create space for the connections
        Vcnn_connection[i].push_back(VViConnections[i].iSize);

        for(i1=0; i1 < VViConnections[i].iSize; i1++){

            //same as at Layer 86 to 102 but with Hidden or Output Neurons as start
            Vcnn_connection[i][i1].iBeg_End[0]=VViConnections[i][i1][0];
            Vcnn_connection[i][i1].iBeg_End[1]=VViConnections[i][i1][1];
            Vcnn_connection[i][i1].iBeg_End[2]=VViConnections[i][i1][2];
            Vcnn_connection[i][i1].iBeg_End[3]=VViConnections[i][i1][3];                
            Vcnn_connection[i][i1].ftdPtrInput= &Vi_N_input[VViConnections[i][i1][0]].ftdOutput;
            Vcnn_connection[i][i1].ftdPtrOutput= &Vh_o_N[VViConnections[i][i1][3]][VViConnections[i][i1][2]].ftdOutput; 
            Vcnn_connection[i][i1].ftdPtrXi= VVftdX[i][VViConnections[i][i1][0]].ValPnt;
            Vcnn_connection[i][i1].ftdPtrXj= VVftdX[VViConnections[i][i1][3]][VViConnections[i][i1][2]].ValPnt;

        }       

    }

    u_chRunNum=0;


    //set every Weight to 0.5 so the neural network can learn
    for(i=0; i < Vcnn_connection.iSize; i++){

        for(i1=0; i1 < Vcnn_connection[i].iSize; i1++){

            Vcnn_connection[i][i1].ftdWeight=0.5;

        }

    }

}

void NNET::neural_network::run(){

    int i;
    int i1;

    for(i=0; i < Vi_N_input.iSize; i++){

        //get the Output for the input
        Vi_N_input[i].set_ftdOutput();
        //save the neuron value
        VVftdX[1][i][u_chRunNum]=Vi_N_input[i].ftdOutput;

    }

    for(i=0; i < Vcnn_connection[0].iSize; i++){

        //aktivate the connections from the biass
        Vcnn_connection[0][i].set_ftdPtrOutput();

    }

    for(i=0; i < Vcnn_connection[1].iSize; i++){

        //aktivate the connections from the input neurons
        Vcnn_connection[0][i].set_ftdPtrOutput();

    }

    for(i=2; i < Vcnn_connection.iSize; i++){

        for(i1=0; i1 < Vh_o_N[i].iSize; i1++){

            //get the output for the hidden or output layer
            Vh_o_N[i][i1].set_ftdOutput();
            //save the neuron value
            VVftdX[i][i1][u_chRunNum]=Vh_o_N[i][i1].ftdOutput;

        }

        for(i1=0; i1 < Vcnn_connection[i].iSize; i1++){

            Vcnn_connection[i][i1].set_ftdPtrOutput();

        }

    }

    u_chRunNum++;

}

void NNET::neural_network::refresh(){

    int i;
    int i1;

    for(i=0; i < Vcnn_connection.iSize; i++){

        for(i1=0; i1 < Vcnn_connection[i].iSize; i++){

            Vcnn_connection[i][i1].set_Weight();

        }

    }

    u_chRunNum=0;

}