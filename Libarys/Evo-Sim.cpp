#include <new>


#define random_int random_int()
#define random_bool random_bool()


D_vector<int> GLOiVfree_places(100);
int GLOifree_index;



void EVO::evolution(const bool &bRun, MAP::map &Evo_Map, C_vector<EVO::creature> &creatures){

    int i;
    int i1;
    //run until main stops it
    while(bRun){
        //run 10 times because of batsh learning
        for(i=0; i < 10; i++){
            //run every creature
            for(i1=0; i1 < creatures.iSize; i1++){
                //run the neural network to get outputs
                creatures[i1].Brain.run();

                //check if the creature want to get a child
                if(creatures[i1].Brain.Vh_o_N[creatures[i1].Brain.Vh_o_N.iSize-1][0].ftdOutput > 1){

                    EVO::birth(Evo_Map, creatures, creatures[i1]);

                }
                //check if the  creature want to eat
                if(creatures[i1].Brain.Vh_o_N[creatures[i1].Brain.Vh_o_N.iSize-1][1].ftdOutput > 1){

                    EVO::eat(Evo_Map, creatures[i1]);

                }
                //check if the creature want to attack
                if(creatures[i1].Brain.Vh_o_N[creatures[i1].Brain.Vh_o_N.iSize-1][2].ftdOutput > 1){

                    EVO::attack(Evo_Map, creatures, creatures[i1]);

                }
                //check if the creature want to move
                if(creatures[i1].Brain.Vh_o_N[creatures[i1].Brain.Vh_o_N.iSize-1][3].ftdOutput > 1){

                    EVO::move(Evo_Map, creatures[i1]);

                }
                //check if the creature want to turn right
                if(creatures[i1].Brain.Vh_o_N[creatures[i1].Brain.Vh_o_N.iSize-1][4].ftdOutput > 1){

                    EVO::turn_right(creatures[i1]);

                }
                //check if the creature want to turn left
                if(creatures[i1].Brain.Vh_o_N[creatures[i1].Brain.Vh_o_N.iSize-1][4].ftdOutput < (0-1)){

                    EVO::turn_left(creatures[i1]);

                }

            }

        }
        //get the new Weights(let the creatures learn)
        for(i=0; i < creatures.iSize; i++){

            creatures[i].Brain.refresh();

        }

    }

}

void EVO::birth(MAP::map &Evo_Map, C_vector<EVO::creature> &creatures, EVO::creature &iMother){

    int i;
    int i1;

    //subtrackt energie needed to birth creature
    iMother.u_chEnergie-=30;

    //check if it is the first or second creature on the field
    if(iMother.bNumonfield){
        //check if there is an other creature on the field
        if(Evo_Map.map_build[iMother.position.x][iMother.position.y].bCreature[0]){
            //do nothing and return because there is no place on the field to birth a child
            return;

        }
        //check if there is still free place left for creatures
        if(GLOifree_index >= 0){

            //create new space
            creatures.push_back(100);
            //save that there is space left
            GLOifree_index=99;
            //save wich space is left
            for(i=0; i < 100; i++){

                GLOiVfree_places[i]=creatures.iSize-i;

            }

        }

    }else{
        //check if there is an other creature on the field
        if(Evo_Map.map_build[iMother.position.x][iMother.position.y].bCreature[1]){
            //do nothing and return because there is no place on the field to birth a child
            return;

        }
        //check if there is still free place left for creatures
        if(GLOifree_index >= 0){

            //create new space
            creatures.push_back(100);
            //save that there is space left
            GLOifree_index=99;
            //save wich space is left
            for(i=0; i < 100; i++){

                GLOiVfree_places[i]=creatures.iSize-i;

            }

        }

    }

    if(random_bool){

        D_vector<int> iVNeurons(iMother.Brain.VVftdX.iSize);
        C_vector<C_vector<D_vector<int>>> Vcnn_connection(iVNeurons.iSize);

        for(i=0; i < iVNeurons.iSize; i++){

            iVNeurons[i]=iMother.Brain.VVftdX[i].iSize;

        }

        iVNeurons[iVNeurons.iSize-1]=iVNeurons[iVNeurons.iSize-2];

        for(i=0; i < iMother.Brain.Vcnn_connection.iSize; i++){

            Vcnn_connection[i].push_back(iMother.Brain.Vcnn_connection[i].iSize);

            for(i1=0; i1 < Vcnn_connection[i].iSize; i1++){

                Vcnn_connection[i][i1].push_back(4);

                Vcnn_connection[i][i1][0]=iMother.Brain.Vcnn_connection[i][i1].iBeg_End[0];
                Vcnn_connection[i][i1][1]=iMother.Brain.Vcnn_connection[i][i1].iBeg_End[1];
                Vcnn_connection[i][i1][2]=iMother.Brain.Vcnn_connection[i][i1].iBeg_End[2];
                Vcnn_connection[i][i1][3]=iMother.Brain.Vcnn_connection[i][i1].iBeg_End[3];

            }

        }

        Vcnn_connection[Vcnn_connection.iSize-1].ValPnt=Vcnn_connection[Vcnn_connection.iSize-1].ValPnt;
        Vcnn_connection[Vcnn_connection.iSize-1].iSize=Vcnn_connection[Vcnn_connection.iSize-1].iSize;
        Vcnn_connection[Vcnn_connection.iSize-2].iSize=0;

        if(random_bool){

            i=random_int%iVNeurons.iSize-1;

            while(i == 2){
            
                i=random_int%iVNeurons.iSize-1;

            }

            iVNeurons[i]++;


            int iTemp1=random_int%Vcnn_connection.iSize;
            int iTemp2=random_int%iVNeurons.iSize;

            while(iTemp2 == 2){
            
                iTemp2=random_int%iVNeurons.iSize;

            }

            Vcnn_connection[iTemp1].push_back(1);

            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1][0]=random_int%iVNeurons[iTemp1];
            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1][1]=iTemp1;
            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1][2]=random_int%iVNeurons[iTemp2];
            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1][1]=iTemp2;



        }else{

            i=random_int%iVNeurons.iSize-1;

            while(i == 2){

                i=random_int%iVNeurons.iSize-1;

            }

            iVNeurons[i]--;

            int iTemp1=random_int%Vcnn_connection.iSize;
            int iTemp2=random_int%Vcnn_connection[iTemp1].iSize;

            Vcnn_connection[iTemp1][iTemp2].deletefront(4);

            for(i=iTemp2; i < Vcnn_connection[iTemp1].iSize; i++){

                Vcnn_connection[iTemp1][i].ValPnt=Vcnn_connection[iTemp1][i+1].ValPnt;

            }

            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1].ValPnt=nullptr;
            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1].~D_vector();

        }

        i=GLOiVfree_places[GLOifree_index];
        GLOifree_index--;

        new (&creatures[i].Brain) NNET::neural_network(iVNeurons, Vcnn_connection);

        if(random_bool){

            creatures[i].iGenetic=iMother.iGenetic+1;

        }else{

            creatures[i].iGenetic=iMother.iGenetic-1;

        }

        creatures[i].u_chEnergie=50;
        creatures[i].direction=0;
        creatures[i].position.x=iMother.position.x;
        creatures[i].position.y=iMother.position.y;
        creatures[i].position_on_field.x=iMother.position_on_field.x;
        creatures[i].position_on_field.y=iMother.position_on_field.y;


    }else{

        D_vector<int> iVNeurons(iMother.Brain.VVftdX.iSize-1);
        C_vector<C_vector<D_vector<int>>> Vcnn_connection(iVNeurons.iSize);

        for(i=0; i < iVNeurons.iSize; i++){

            iVNeurons[i]=iMother.Brain.VVftdX[i].iSize-1;

        }

        for(i=0; i < iMother.Brain.Vcnn_connection.iSize; i++){

            Vcnn_connection[i].push_back(iMother.Brain.Vcnn_connection[i].iSize);

            for(i1=0; i1 < Vcnn_connection[i].iSize; i1++){

                Vcnn_connection[i][i1].push_back(4);

                Vcnn_connection[i][i1][0]=iMother.Brain.Vcnn_connection[i][i1].iBeg_End[0];
                Vcnn_connection[i][i1][1]=iMother.Brain.Vcnn_connection[i][i1].iBeg_End[1];
                Vcnn_connection[i][i1][2]=iMother.Brain.Vcnn_connection[i][i1].iBeg_End[2];
                Vcnn_connection[i][i1][3]=iMother.Brain.Vcnn_connection[i][i1].iBeg_End[3];

            }

        }

        if(random_bool){

            i=random_int%iVNeurons.iSize-1;

            while(i == 2){
            
                i=random_int%iVNeurons.iSize-1;

            }

            iVNeurons[i]++;


            int iTemp1=random_int%Vcnn_connection.iSize;
            int iTemp2=random_int%iVNeurons.iSize;

            while(iTemp2 == 2){
            
                iTemp2=random_int%iVNeurons.iSize;

            }

            Vcnn_connection[iTemp1].push_back(1);

            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1][0]=random_int%iVNeurons[iTemp1];
            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1][1]=iTemp1;
            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1][2]=random_int%iVNeurons[iTemp2];
            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1][1]=iTemp2;



        }else{

            i=random_int%iVNeurons.iSize-1;

            while(i == 2){

                i=random_int%iVNeurons.iSize-1;

            }

            iVNeurons[i]--;

            int iTemp1=random_int%Vcnn_connection.iSize;
            int iTemp2=random_int%Vcnn_connection[iTemp1].iSize;

            Vcnn_connection.deletefront(4);

            for(i=iTemp2; i < Vcnn_connection[iTemp1].iSize; i++){

                Vcnn_connection[iTemp1][i].ValPnt=Vcnn_connection[iTemp1][i+1].ValPnt;

            }

            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1].ValPnt=nullptr;
            Vcnn_connection[iTemp1][Vcnn_connection[iTemp1].iSize-1].~D_vector();

        }

        i=GLOiVfree_places[GLOifree_index];
        GLOifree_index--;

        new(&creatures[i].Brain) NNET::neural_network(iVNeurons, Vcnn_connection);

        if(random_bool){

            creatures[i].iGenetic=iMother.iGenetic+1;

        }else{

            creatures[i].iGenetic=iMother.iGenetic-1;

        }

        creatures[i].u_chEnergie=50;
        creatures[i].direction=0;
        creatures[i].position.x=iMother.position.x;
        creatures[i].position.y=iMother.position.y;
        creatures[i].position_on_field.x=iMother.position_on_field.x;
        creatures[i].position_on_field.y=iMother.position_on_field.y;

    }



}

void EVO::eat(MAP::map &Evo_Map, EVO::creature &eating){

    int i;
    //check if the foodvalueis smaller then 20
    if(Evo_Map.map_build[eating.position.x][eating.position.y].u_chfoodvalue < 20){
        //add the foodvalue to the energy of the creature and set the foodvalue to 0
        eating.u_chEnergie+=Evo_Map.map_build[eating.position.x][eating.position.y].u_chfoodvalue;
        Evo_Map.map_build[eating.position.x][eating.position.y].u_chfoodvalue=0;

        return;

    }
    //add 20 energy to the creature and subtract 20 foodvalue from the field
    eating.u_chEnergie+=20;
    Evo_Map.map_build[eating.position.x][eating.position.y].u_chfoodvalue-=20;
    
}

void EVO::attack(MAP::map &Evo_Map, C_vector<EVO::creature> creatures, EVO::creature &attacker){
    //subtract energy for an action
    attacker.u_chEnergie-=10;
    //check on wich space is free
    if(attacker.bNumonfield){
        //check if there is a creature on the field
        if(Evo_Map.map_build[attacker.position.x][attacker.position.y].bCreature[0]){
            //subtract energy from the attacked creature
            creatures[Evo_Map.map_build[attacker.position.x][attacker.position.y].iCreature[0]].u_chEnergie-=20;

        }

    }else{
        //check if there is a creature on the field
        if(Evo_Map.map_build[attacker.position.x][attacker.position.y].bCreature[1]){
            //subtract energy from the attacked creature
            creatures[Evo_Map.map_build[attacker.position.x][attacker.position.y].iCreature[1]].u_chEnergie-=20;

        }

    }

}

void EVO::move(MAP::map &Evo_Map, EVO::creature &walking){
    //subtract energy for an action
    walking.u_chEnergie--;
    //get the direction
    switch(walking.direction){
        //walk right
        case 0:
        walking.position_on_field.x++;
        break;
        //walk right and up
        case 1:
        walking.position_on_field.x++;
        walking.position_on_field.y++;
        break;
        //walk up
        case 2:
        walking.position_on_field.y++;
        break;
        //walk left and up
        case 3:
        walking.position_on_field.x--;
        walking.position_on_field.y++;
        break;
        //walk left
        case 4:
        walking.position_on_field.x--;
        break;
        //walk left and down
        case 5:
        walking.position_on_field.x--;
        walking.position_on_field.y--;
        break;
        //walk down
        case 6:
        walking.position_on_field.y--;
        break;
        //walk right and down
        case 7:
        walking.position_on_field.x++;
        walking.position_on_field.y--;
        break;

    }
    //check if the creature moves right a field
    if(walking.position_on_field.x > 9){
        //check if on the next field is free place
        if(Evo_Map.map_build[walking.position.x+1][walking.position.y].bCreature[0] == false){
            //check wich place on the field it leaves
            if(walking.bNumonfield){
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=false;

            }else{
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=false;

            }
            //change the position
            walking.position.x++;
            walking.position_on_field.x=0;
            //set on this place of the field is now a creature
            Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=true;

        }else if(Evo_Map.map_build[walking.position.x+1][walking.position.y].bCreature[1] == false){
            //check wich place on the field it leaves
            if(walking.bNumonfield){
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=false;

            }else{
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=false;

            }
            //change the position
            walking.position.x++;
            walking.position_on_field.x=0;
            //set on this place of the field is now a creature
            Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=true;

        }else{
            //set the x value to a possible worth
            walking.position_on_field.x--;

        }

    }
    //check if the creature moves up a field
    if(walking.position_on_field.y > 9){
        //check if on the next field is free place
        if(Evo_Map.map_build[walking.position.x][walking.position.y+1].bCreature[0] == false){
            //check wich place on the field it leaves
            if(walking.bNumonfield){
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=false;

            }else{
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=false;

            }
            //change the position
            walking.position.y++;
            walking.position_on_field.y=0;
            //set on this place of the field is now a creature
            Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=true;

        }else if(Evo_Map.map_build[walking.position.x][walking.position.y+1].bCreature[1] == false){
            //check wich place on the field it leaves
            if(walking.bNumonfield){
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=false;

            }else{
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=false;

            }
            //change the position
            walking.position.y++;
            walking.position_on_field.y=0;
            //set on this place of the field is now a creature
            Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=true;

        }else{
            //set the y value to a possible worth
            walking.position_on_field.y--;

        }

    }
    //check if the creature moves left a field
    if(walking.position_on_field.x < 0){
        //check if on the next field is free place
        if(Evo_Map.map_build[walking.position.x-1][walking.position.y].bCreature[0] == false){
            //check wich place on the field it leaves
            if(walking.bNumonfield){
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=false;

            }else{
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=false;

            }
            //change the position
            walking.position.x--;
            walking.position_on_field.x=9;
            //set on this place of the field is now a creature
            Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=true;

        }else if(Evo_Map.map_build[walking.position.x-1][walking.position.y].bCreature[1] == false){
            //check wich place on the field it leaves
            if(walking.bNumonfield){
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=false;

            }else{
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=false;

            }
            //change the position
            walking.position.x--;
            walking.position_on_field.x=9;
            //set on this place of the field is now a creature
            Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=true;

        }else{
            //set the x value to a possible worth
            walking.position_on_field.x++;

        }

    }
    //check if the creature moves down a field
    if(walking.position_on_field.y < 0){
        //check if on the next field is free place
        if(Evo_Map.map_build[walking.position.x][walking.position.y-1].bCreature[0] == false){
            //check wich place on the field it leaves
            if(walking.bNumonfield){
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=false;

            }else{
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=false;

            }
            //change the position
            walking.position.y--;
            walking.position_on_field.y=9;
            //set on this place of the field is now a creature
            Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=true;

        }else if(Evo_Map.map_build[walking.position.x][walking.position.y-1].bCreature[1] == false){
            //check wich place on the field it leaves
            if(walking.bNumonfield){
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=false;

            }else{
                //set on this place of the field is no creature
                Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[0]=false;

            }
            //change the position
            walking.position.y--;
            walking.position_on_field.y=9;
            //set on this place of the field is now a creature
            Evo_Map.map_build[walking.position.x][walking.position.y].bCreature[1]=true;

        }else{
            //set the y value to a possible worth
            walking.position_on_field.y++;

        }

    }

}

void EVO::turn_left(EVO::creature &turning){
    //check if the direction gets 7 if it turns left
    if(turning.direction == 0){
        //turn the creature left
        turning.direction=7;

    }else{
        //turn the creature left
        turning.direction--;

    }

}

void EVO::turn_right(EVO::creature &turning){
    //check if the direction gets 0 if it turns right
    if(turning.direction == 7){
        //turn the creature right
        turning.direction=0;

    }else{
        //turn the creature right
        turning.direction++;

    }

}

void EVO::die(MAP::map &Evo_Map, C_vector<EVO::creature> creatures, EVO::creature &die, int &iDie){

    if(GLOifree_index == GLOiVfree_places.iSize){

        GLOiVfree_places.push_back(101);

    }

    if(die.bNumonfield){

        Evo_Map.map_build[die.position.x][die.position.y].bCreature[1]=false;

    }else{

        Evo_Map.map_build[die.position.x][die.position.y].bCreature[1]=false;

    }

    GLOifree_index++;
    GLOiVfree_places[GLOifree_index]=iDie;

    die.Brain.~neural_network();

}