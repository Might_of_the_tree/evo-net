namespace NNET{

    struct input_N{

        double* ftdPtrInput;
        double  ftdOutput;

        void set_ftdOutput();

    };

    struct hidden_output_N{

        double ftdOutput;

        D_vector<double> VftdInput;

        hidden_output_N();

        void set_ftdOutput();

    };

    struct connection{

        int iBeg_End[4];

        double ftdWeight;

        double* ftdPtrInput ;
        double* ftdPtrOutput;
        double* ftdPtrXi    ;
        double* ftdPtrXj    ;

        void set_Weight      ();
        void set_ftdPtrOutput();

    };

    
    class neural_network{
    private:

        unsigned char u_chRunNum;

    public:

        D_vector<double> ftdBias;
        C_vector<input_N> Vi_N_input;
        C_vector<C_vector<hidden_output_N>> Vh_o_N;
        C_vector<C_vector<connection>> Vcnn_connection;
        C_vector<C_vector<D_vector<double>>> VVftdX;

        neural_network(D_vector<int> &VNeurons, C_vector<C_vector<D_vector<int>>> &VViConnections);
        neural_network(){};

        void run();
        void refresh();

    };

}